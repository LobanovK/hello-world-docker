FROM node:15-slim as build

COPY src src
COPY package.json package.json
COPY public public

RUN npm i
RUN npm run build

FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /opt/frontend
COPY --from=build /build /opt/frontend
